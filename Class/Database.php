<?php


class Database{
    
    private $db_name;
    private $db_user;
    private $db_pass;
    private $db_host;
    private $pdo;
    
    //initialise les information pour se connecter à la bdd
    public function __construct($db_name,$db_user='root',$db_pass='',$db_host='localhost'){
        
        $this->db_name=$db_name;
        $this->db_user=$db_user;
        $this->db_pass=$db_pass;
        $this->db_host=$db_host;
        
        
    }
    //return une variable de type pdo connecté à la bdd repair
    private function getPDO(){
     
    if($this->pdo === null){
       $pdo =new PDO('mysql:dbname=repair;host=localhost','root','');
       $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
       $this->pdo=$pdo;
        }
       return $this->pdo;
    }
    //permet de faire un query sur un objet PDO puis de retourner chaque ligne de la bdd sous forme d'un ARRAY
    public function query($statement){
        $req= $this->getPDO()->query($statement);
        $datas= $req->fetchALL(PDO::FETCH_OBJ);
        return $datas;
    }
    //permet d'executer une fonction sql et de retourner son contenu
    public function exec($statement){
        $datas= $this->getPDO()->exec($statement);
        return $datas;
    }
    
}

?>
