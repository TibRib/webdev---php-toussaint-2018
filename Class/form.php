

<?php


class Form{
    
    
    private $data;
    
    public function __construct($data =array()){
        
        $this->data=$data;
    }
    
    
    //Permet de créer une balise label + input  
    public function input($label,$name,$placeholder,$class,$value=null,$type='text',$pattern=null){
        
       
        if(isset($label)){ echo '<label for="'.$name.'"> '.$label.' </label>' ; }
            if($pattern != null){
                echo '<input type="'.$type.'" class="'.$class.'" name="'.$name.'" id="' .$name.'" placeholder="'.$placeholder.'" value="'.$value.'" pattern="'.$pattern.'" required></input>';
            }
            else{
                echo '<input type="'.$type.'" class="'.$class.'" name="'.$name.'" id="' .$name.'" placeholder="'.$placeholder.'" value="'.$value.'" required></input>';
            }
        }
     //Permet de créer une balise sélect avec les options associées   
    public function select($label,$name,$class,$options,$Onclick){
        //$options est un array (tableau) des differentes options.
        if(isset($label)){ echo '<label> '.$label.' </label>' ; }
        echo '<select name="'.$name.'" class="'.$class.'" id="'.$name.'" onChange="'.$Onclick.'">';
        
        //$options[1] contient $opt = array("txt","value");
        //Ainsi, $options[1][0] = $opt[0] = texte option 1   & $options[1][1] = $opt[1] = value option 1.
        foreach($options as $opt){
            
            echo '<option value="'.$opt[1].'">'.$opt[0].'</option>';
        }
        
        echo '</select>';
    }
    
    //Permet de creer un bouton
    public function submit($class,$value,$Onclick,$type='button'){
        
        echo '<br><button type="'.$type.'" onclick="'.$Onclick.'"  class="'.$class.'">'.$value.'</button>';
    }
}



?>


        
