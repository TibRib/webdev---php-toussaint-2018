<?php
    //Deconnexion de l'utilisateur puis redirection
    session_start(); 
    $_SESSION["login"] = false;
    
    header('Location: index.php');
    ?>