<?php


//Verification de la session.
session_start();
if(isset($_SESSION["login"])){}
else{
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"]===false){
    header("Location: pages/sign_in.php");
    die();
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Accueil | Repair</title>
        
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
        <link rel="icon" href="img/favicon.ico" />
        <link rel="stylesheet" href="css/styles.css" />
    </head>
    <body>
        
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Le label et le menu déroulant resteront groupés pour affichage mobile -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                      <span class="sr-only">Naviguer</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Repair</a>
                  </div>

                  <!-- Contenu -->
                  <div class="collapse navbar-collapse" id="collapse-1">
                      <!-- Liste des boutons clickables du menu -->
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Accueil</a></li>

                      <!-- Element déroulant : class = "dropdown" -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Licences <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="pages/licences/envoyer_cle.php">Envoyer une clé</a></li>
                          <li><a href="pages/licences/obtenir_cle.php">Obtenir une clé</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="pages/licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                        </ul>
                      </li>
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Stocks <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="pages/stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="pages/stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="pages/stocks/inventaire_pc.php">Inventaire des PC</a></li>
                        </ul>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Demandes <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="pages/demandes/ajout_demande.php">Soumettre une demande</a></li>
                            <li><a href="pages/demandes/terminer_demande.php">Valider une demande</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href='pages/demandes/inventaire_archive.php'>Archive des envois</a></li>
                        </ul>
                      </li>
                      
                      <li><a href="pages/timeline.php">Suivi & Historique</a></li>         
                      <li><a href="https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR">
                              <img style="max-width: 20px" src="img/Microsoft-Teams.png" alt="Microsoft" />
                              Teams
                          </a></li>    
                    </ul>
                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="pages/inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="logout.php" id="imgout"><img src="img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>


                      
                  </div>
                </div>
              </nav>
            
            <div class="jumbotron">
                <img id="indexpic" class="center-block" src="img/logo_repair_big.png" alt="ISEN Repair"/>
                <br>
                <p class="text-center">
                    <b>Bienvenue sur le site interne de l'ISEN Repair.</b>
                </p>
                <p class="text-center">
                    Ici, tu trouveras plusieurs liens utiles à tes missions au sein de l'association.
                    
                </p>
                <hr>
               
                <!-- Sommaire -->
                <div class="row">
                    <div class="col col-md-3">
                        <h2>Licences</h2>
                        <ul>
                            <li><a href="pages/licences/envoyer_cle.php">Envoyer une clé</a></li>
                            <li><a href="pages/licences/obtenir_cle.php">Obtenir une clé</a></li>
                            <li><a href="pages/licences/inventaire_cle.php">Inventaire des clés</a></li>
                        </ul>
                    </div>
                    <div class="col col-md-3">
                        <h2>Stocks</h2>
                        <ul>
                            <li><a href="pages/stocks/enregistrer_pc.php">Enregistrer un pc</a></li>
                            <li><a href="pages/stocks/demandes_et_dispo.php">Matériel prêt / à préparer</a></li>
                            <li><a href="pages/stocks/inventaire_pc.php">Inventaire des pc</a></li>
                        </ul>
                    </div>
                    <div class="col col-md-3">
                        <h2>Demandes</h2>
                        <ul>
                            <li ><a href="pages/demandes/ajout_demande.php">Ajouter une demande</a></li>
                            <li><a href="pages/demandes/terminer_demande.php">Terminer une demande</a></li>
                            <li><a href="pages/demandes/inventaire_archive.php">Inventaire des pc livrés</a></li>
                        </ul>
                    </div>
                    <div class="col col-md-3">
                        <h2>Historique</h2>
                        <ul>
                            <li><a href="pages/timeline.php">Suivi annuel des livraisons</a></li>
                            <li><a href="pages/timeline/ajout_event.php">Inscrire une livraison</a></li>
                        </ul>
                    </div>
                </div>  <!-- Fin Sommaire -->
                
                
            </div> <!-- Fin jumbotron -->
        </div> <!-- Fin container -->
        
         <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        // animation du fondu en chargement de l'image
            document.getElementById("indexpic").style.opacity = '1';
            document.getElementById("indexpic").style.transform = 'opacity 2s';
        </script>
    </body>
</html>
