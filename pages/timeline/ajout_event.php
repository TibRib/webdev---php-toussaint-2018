<?php
//Verification de la session.
session_start();
if (isset($_SESSION["login"])) {
    
} else {
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"] === false) {
    header("Location: ../sign_in.php");
    die();
}

//Si le role est trop bas
if ($_SESSION["role"] < 2) {
    header("Location: ../non_autorise.php");
    die();
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ajouter un Event | Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <?php
        require '../../Class/form.php';
        $form = new Form($_POST);
        ?>

        <script src="../../js/ObjetXHR.js" type="text/javascript"></script>

        <script type="text/javascript">
            function request(callback) {
          //Verifie si tous les champs sont remplis correctement puis récupère la valeur de chaque champ
            //Envoie toutes ces informations dans envoieeventbdd.php pour le traitement en affichant l'image de chargement puis une div avec le message reçu(supprimer à chaque envoie)

                if (document.getElementById('InputEventDest').checkValidity() && document.getElementById('InputEventContenu').checkValidity()) {

                    var dest = document.getElementById("InputEventDest").value;
                    var contenu = document.getElementById("InputEventContenu").value;
                    var DateEvent = document.querySelector("input[name='InputDateEvent']").value
                    var xhr = getXMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                            callback(xhr.responseText);
                            document.getElementById("image").style.display = "none";
                        } else if (xhr.readyState < 4) {
                            document.getElementById("image").style.display = "inline";
                            if (document.getElementById("reponsexhr")) {
                                var div = document.getElementById("reponsexhr");
                                var parent = document.querySelector('.jumbotron');
                                parent.removeChild(div);
                            }

                        }
                    };
                    xhr.open("POST", "envoieeventbdd.php", true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send('InputEventDest=' + dest + '&InputEventContenu=' + contenu + '&InputDateEvent=' + DateEvent);
                } else {
                    alert("Remplissez correctement le formulaire ;)")
                }
            }
             //Permet de creer la div pour contenir le message reçu après traitement
            function testAlert(text) {

                var DivJum = document.querySelector('.jumbotron');
                var newDiv = document.createElement('div');
                newDiv.className = 'form-group col-md-6 col-centered MarginTop';
                newDiv.id = 'reponsexhr';
                var newP = document.createElement('p');
                newP.className = 'text-center';
                newDiv.appendChild(newP);

                var newtexte = document.createTextNode(text);
                newP.appendChild(newtexte);
                DivJum.appendChild(newDiv);


            }

        </script>

    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../licences/envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="../licences/obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Demandes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../demandes/ajout_demande.php">Soumettre une demande</a></li>
                                    <li><a href="../demandes/terminer_demande.php">Valider une demande</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href='../demandes/inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                         <ul class="nav navbar-nav" id="lieninscr">
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"></a>
                          </li>
                        </ul>


                    </div>
                </div>
            </nav>

            <div class="jumbotron">

                <h1 class="text-center">Enregistrer une livraison</h1>
                <p class='text-center' style='font-size: medium;'>Cet élément (livraison ou autre) sera affiché dans la <a href='../timeline.php'>timeline</a></p>
                <form>

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('Entrez la destination', 'InputEventDest', 'Ex: Ecole Jean-Moulin - Toulon', 'form-control');
                        ?>
                    </div>

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('Contenu de la livraison', 'InputEventContenu', 'Ex: 4 pc fixes avec claviers, souris et écrans', 'form-control');
                        ?>
                        <!--                      <label for="InputMarque">Préciser la marque du PC</label>
                                              <input type="text" class="form-control" id="InputMarque" placeholder="Ex: HP; DELL; Custom">-->
                    </div>

                    <div class="form-group col-md-6 col-centered MarginTop">

                        <label for="InputDateEvent">Entrer la date de l'évènement ou laisser vide si aujourd'hui:</label>
                        <input placeholder=<?php
                        date_default_timezone_set('Europe/Paris');
                        echo '"';
                        echo date('d') . '/' . date('m') . '/' . date('y');
                        echo '"';
                        ?> class="form-control" type="text" onfocus="(this.type = 'date')" name="InputDateEvent">
                    </div>

                    <div class="text-center MarginTop">
                      <?php
                        $form->submit('btn btn-primary', 'Ajouter l\'Event', 'request(testAlert)');
                        ?>
                        <!--                    <button type="submit" class="btn btn-primary">Inscrire ce PC</button>-->
                    </div>
                     <div class="form-group col-md-3 col-centered MarginTop">
                    <span id="image" style="display:none"><img src="../../img/loader.svg" alt="loading"/></span>
                    </div>
                </form>

            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
