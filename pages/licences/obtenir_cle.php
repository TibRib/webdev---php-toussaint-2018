<?php

//Verification de la session.
session_start();
if(isset($_SESSION["login"])){}
else{
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"]===false){
    header("Location: ../sign_in.php");
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
         <?php
        require '../../Class/form.php';
        $form=new Form($_POST);
        ?>
                <script>
        function copyText() {
          var copy_text = document.getElementById("to-copy");
          var range = document.createRange();
          range.selectNode(copy_text);
          window.getSelection().addRange(range);

          try {
            var successful = document.execCommand('copy');
            if(successful){
                document.getElementById("copy-confirm").innerHTML = "Clé copiée dans le presse papier <span class='text-success'>✔</span> 📋 ";
                
            }
            else {
                document.getElementById("copy-confirm").innerHTML = "Clé non copiée... ";
            }
            console.log('Copying text command was ' + msg);
          } catch (err) {
            console.log('Oops, unable to copy');
          }

        }
            </script>
    </head>

    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                      <span class="sr-only">Naviguer</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../../index.php">Repair</a>
                  </div>

                  <!-- Contenu -->
                  <div class="collapse navbar-collapse" id='collapse-1'>
                      <!-- Liste des boutons clickables du menu -->
                    <ul class="nav navbar-nav">
                        <li><a href="../../index.php">Accueil</a></li>

                      <!-- Element déroulant : class = "dropdown" -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Licences <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="envoyer_cle.php">Envoyer une clé</a></li>
                          <li class="active"><a href="obtenir_cle.php">Obtenir une clé</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                        </ul>
                      </li>
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Stocks <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="../stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="../stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="../stocks/inventaire_pc.php">Inventaire des PC</a></li>
                        </ul>
                      </li>
                      
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Demandes <span class="caret"></span></a>
                         <ul class="dropdown-menu">
                             <li><a href="../demandes/ajout_demande.php">Soumettre une demande</a></li>
                             <li><a href="../demandes/terminer_demande.php">Valider une demande</a></li>
                         <li role="separator" class="divider"></li>
                             <li><a href='../demandes/inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>

                      
                  </div>
                </div>
              </nav>

            <div class="jumbotron">
                <h1 class="text-center">Obtenir une clé de licence</h1>
                <form method="post">
                    <fieldset class="form-group MarginTop">
                        <legend class="text-center">Choisissez votre version de Windows</legend>
                        <div class="col-md-5 col-centered">
                            <!-- Clés anciennes (Legacy) -->
                            <div class="col-md-6">
                                <div class="form-check">
                                    <?php
                                    $form->input('Windows XP','optionsRadios','','form-check-input','Windows XP','radio');
                                    
                                    ?>
                                </div>
                                <div class="form-check">
                                     <?php
                                   $form->input('Windows Vista','optionsRadios','','form-check-input','Windows Vista','radio');
                                    
                                    ?>
                                </div>
                            </div>
                            
                            <!-- Clés récentes (Actual) -->
                            <div class="col-md-6">
                                <div class="form-check">
                                     <?php
                                    $form->input('Windows 7','optionsRadios','','form-check-input','Windows 7','radio');
                                    
                                    ?>
                                </div>
                                <div class="form-check">
                                     <?php
                                     $form->input('Windows 8','optionsRadios','','form-check-input','Windows 8','radio');
                                    
                                    ?>
                                </div>
                                <div class="form-check">
                                     <?php
                                     $form->input('Windows 10','optionsRadios','','form-check-input','Windows 10','radio');
                                    
                                    ?>
                                </div>
                            </div>
                        </div>
                      </fieldset>

                    <div class="text-center MarginTop">
                          <?php
                        $form->submit('btn btn-primary','Recevoir une clé','','submit');
                        ?>
                    </div>
                    

                    <div class="form-group col-md-6 col-centered MarginTop">



                        <?php
                        // Affiche la cle selon la version de windows choisie et la date d'enregistrement 
                        //La date d'utilisation vaut la date au moment de l'obtention de la clé
                        require '../../Class/Database.php';
                        $db = new Database('repair');
                        $btncopy = 0;
                        if (isset($_POST['optionsRadios'])) {
                            $version = $_POST['optionsRadios'];
                            date_default_timezone_set("Europe/Paris");
                            $date = date("Y-m-d");
                            $heure = date("H:i:s");


                            $row = $db->query('SELECT * FROM clewindows WHERE version="' . $version . '" ORDER BY date_utilisation');
                               

                            //  var_dump($row);
                            if(isset($row[0])){
                            echo '<label for="InputKey">Voici votre clé de licence:</label>';
                            echo '<p id="to-copy" class="text-center text-primary">'.$row[0]->Nom.'</p>';
                            $btncopy = 1;
                            $db->exec('UPDATE clewindows SET date_utilisation="' . $date . ' ' . $heure . '" WHERE id=' . $row[0]->id . '');
                            }
                            else{
                                echo '<p class="text-center"> Il n\'y pas de clé existante </p>';
                            }
                        }
                        ?>
                        
                        <p class="text-center" id="copy-confirm"></p>
                    </div>
                </form>
                   <?php if($btncopy == 1){ echo '<button class="btn btn-primary copybtn" onclick="copyText()">Copy</button>'; } ?>

            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
        

    </body>
</html>
