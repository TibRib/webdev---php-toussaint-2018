<?php
//Verification de la session.
session_start();
if (isset($_SESSION["login"])) {
    
} else {
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"] === false) {
    header("Location: ../sign_in.php");
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <link rel="stylesheet" href="../../css/inventaire.css" />
        
        <script src="../../js/ObjetXHR.js" type="text/javascript"></script>
<?php if($_SESSION["role"] > 0) :?>
        <script type="text/javascript">
            //Bouton de suppression
            function request(id) {
                {
                    var Id = id;
                    var xhr = getXMLHttpRequest();
                    
                    if(confirm("Confirmer la suppression de la clé n°"+ Id +" ?"))
                    {
                        xhr.open("POST", "supprimeCle.php", true);
                        var params = 'Id=' + Id ;
                        
                        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

                        xhr.onreadystatechange = function() {
                            if(xhr.readyState == 4 && xhr.status == 200) {
                                alert(xhr.responseText);
                            }
                        }
                        xhr.send(params);

                        console.log(xhr.responseText);
                        document.location.reload(true);
                    }
                }

            }


        </script>
<?php endif; ?>
    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="active"><a href="inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Demandes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../demandes/ajout_demande.php">Soumettre une demande</a></li>
                                    <li><a href="../demandes/terminer_demande.php">Valider une demande</a></li>
                                <li role="separator" class="divider"></li>
                             <li><a href='../demandes/inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>


                    </div>
                </div>
            </nav>

            <div class="jumbotron">
                <h1>Inventaire des clés Windows</h1>
                <p>Vous trouverez ici-bas l'ensemble des clés à l'image de notre base de données.</p>
                <p>
                    Ces clés sont classées selon la version de Windows, la date d'enregistrement, la validité et la date de dernière utilisation connue. 
                </p>

                <!-- Tableau s'adaptant a la fenetre (si trop de clés, on scroll) -->
                <div style="overflow-y:auto;">
                    <?php
                    require "../../Class/Database.php";
                    $pdo = new Database('repair');
                    //Affiche l'Inventaire sous forme de tableau des clés envoyées
                    function afficheInvent($Nom_champ, $Table) {

                        $pdo = new Database('repair');

                        echo '<table id="inventaire" class="col-md-8 col-centered" border="1" cellpadding="2">
                         <thead>
                        <tr>
                        <th><b>Version</b></th>
                        <th class="col-md-4"><b>Clé</b></th>
                        <th><b>Enregistrement</b></th>
                        <th><b>Dernière utilisation</b></th>';
                        if($_SESSION["role"] > 0){
                         echo '<th><b>Suppression</b></th>';
                        }
                        echo '
                        </tr>
                        </thead>
                        <tbody>';
                        foreach ($pdo->query('SELECT * FROM ' . $Table . '  ORDER BY version,' . $Nom_champ . ' ')as $row) {
                            $id = $row->id;
                            $version = $row->version;
                            $nom = $row->Nom;
                            $enregistrement = explode(" ", $row->date_enregistrement)[0];//Pour afficher seulement la date sans l'heure
                            $utilisation = explode(" ", $row->date_utilisation)[0];//Pour afficher seulement la date sans l'heure
                            echo'<tr>
                            <td>' . $version . '</td>
                            <td>' . $nom . '</td>
                            <td>' . $enregistrement . '</td>
                            <td>' . $utilisation . '</td>';
                            if($_SESSION["role"] > 0){
                                echo '<td style="cursor: pointer" onclick="request(' . $id . ')" id="delete' . $id . '">🗑️</td>';
                            }
                            echo '</tr>';
                        }
                       
                        echo '</tbody>
                              </table>';
                    }

                    afficheInvent("date_utilisation", "clewindows")
                    ?>

                </div>


            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
