<?php

//Verification de la session.
session_start();
if(isset($_SESSION["login"])){}
else{
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"]===false){
    header("Location: sign_in.php");
    die();
}

    //Creation d'un pdo qui appelle la base repair
    require "../Class/Database.php";
    $db= new Database('repair');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>

        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/styles.css" />
        <link rel="stylesheet" href="../css/timeline.css" />

    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="licences/envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="licences/obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="stocks/inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  Demandes <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                  <li><a href="demandes/ajout_demande.php">Soumettre une demande</a></li>
                                  <li><a href="demandes/terminer_demande.php">Valider une demande</a></li>
                                  <li role="separator" class="divider"></li>
                                  <li><a href='demandes/inventaire_archive.php'>Archive des envois</a></li>
                                </ul>
                            </li>
                      
                            <li class="active"><a href='timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../logout.php" id="imgout"><img src="../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>


                    </div>
                </div>
            </nav>

            <div class='jumbotron'>
                <h1 class='h2 text-center'>Suivi des dons</h1>
                    <hr width='38%'>
                <h2 class='h3 text-center'>Les chiffres</h2>
                    <hr width='10%'>
                <div class='text-center'>
                    <ul style='list-style-type: none;'>
                        <li>
                            <span class='h2' style='color:green;'>
                                <?php
                                    $nbp = $db->query("SELECT COUNT(Id) AS somme FROM archivepc")[0];
                                    if(isset($nbp->somme)){
                                        echo $nbp->somme;
                                    }
                                     else{
                                         echo '0';
                                     }
                                     //On affiche le nombre de Pc Envoyé par l'association ce qui correspond au nombre de pc archivés
                                ?>
                            </span>
                            <span class='h3'>PCs livrés cette année</span>
                        </li>
                        <li>
                            <span class='h2' style='color:green;'>
                                <?php
                                $req = $db->query("SELECT COUNT(Id) AS count FROM livraison")[0];
                                echo $req->count;
                                // On affiche le nombre d'association aidée par l'Isen repairI
                                ?>
                            </span>
                            <span class='h3'>Associations aidées</span>             
                        </li>
                    </ul>
                </div>
                    <hr width='38%'>
                
                <div class="timeline" style='margin-top: 4em'>
                    <h2 class='h3 text-center'>
                        Timeline
                    </h2>
                <?php if($_SESSION["role"] > 1) :?>
                    <p class='text-center'>
                        <a class='btn btn-primary' href='timeline/ajout_event.php'><span class='text-center'>Ajouter un évenement</span></a>
                    </p>
                <?php endif; ?>
                    <?php
                        
                        $cpt = 1;
                        
                        foreach ($db->query("SELECT * FROM livraison ORDER BY date DESC") as $req) {
                            $cpt++;
                            
                            if(($cpt)%2){$cote = "right";} else{$cote ="left";}
                            echo "<div class='row'><div class='col-md-5 box ".$cote."'>";

                            echo '<h3>Livraison vers '.$req->destination.'</h3>';
                            echo '<h4>Livraison du '.$req->date.'</h4>';
                            echo '<p>'.$req->contenu.'</p>';
                            
                            echo '</div>';
                            echo '<div class="vl'.$cote.'"></div></div>';
                        }
                        
                    
                    ?>
                </div>

            </div>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
