<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Connexion | Repair</title>
        <link rel="icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
        <link href="../css/signin.css" rel="stylesheet">
    </head>

  <body class="text-center">
      
      <!--Formulaire de connection qui redirige vers valideSession.php-->
    <form class="form-signin" action="../valideSession.php" method="post">
      <img class="mb-4" src="../img/logo_repair_big.png" alt="ISEN REPAIR" height="132">
      <label for="inputName" class="sr-only">Nom</label>
      <input type="text" id="inputName" class="form-control" placeholder="Nom" name="nom" required autofocus pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$">
      <label for="inputPassword" class="sr-only">Mot de passe</label>
      <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Mot de passe" required pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$">
      <button class="btn btn-lg btn-primary btn-block" type="submit" name="sub">Connexion</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
    </form>
      
  </body>

</html>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
