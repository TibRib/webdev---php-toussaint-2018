<?php
//Verification de la session.
session_start();
if (isset($_SESSION["login"])) {
    
} else {
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"] === false) {
    header("Location: ../sign_in.php");
    die();
}
//Si le role est trop bas
if ($_SESSION["role"] < 2) {
    header("Location: ../non_autorise.php");
    die();
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <link rel="stylesheet" href="../../css/inventaire.css" />

        <script src="../../js/ObjetXHR.js" type="text/javascript"></script>

        <script type="text/javascript">
            var cpt=0; //Variable compteur globale : compte du nombre de checkboxes cochées
            //Creation d'un tableau qui va contenir les Ids de chaque Pc sélectionné
            //Recupère la valeur du Sélect.
            //Envoie toutes ces informations dans Archivepcbdd.php pour le traitement en affichant l'image de chargement puis une div avec le message reçu(supprimée à chaque envoie)
            
            function request(){
               
                 var idpc=new Array();
                var nbpcdispo=document.getElementById('nbpcdispo').innerHTML;
    
                var liste;
                liste = document.getElementById("SelectDemande");
                option = liste.options[liste.selectedIndex].text;
                for(i=0,j=0;i<nbpcdispo;i++)
                {
                    if(document.getElementById('check'+(i+1)).checked===true){
                    idpc[j]=document.getElementById('idligne'+(i+1)).innerHTML;
                    
                    j++;
                    
                }
                }
                
                
                 var xhr = getXMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                          document.getElementById("image").style.display = "none";
                          testAlert(xhr.responseText);
                          setTimeout(function() {
                         // Après une seconde :
 
                       if(xhr.responseText=='Archivation effectuée')
                       {
                           
                           window.location.replace("terminer_demande.php");
                       }
                         }, 1500);

                    } else if (xhr.readyState < 4) {
                        
                         document.getElementById("image").style.display = "inline";
                        if(document.getElementById("reponsexhr")){
                            var div=document.getElementById("reponsexhr");
                            var parent=document.querySelector('.jumbotron');
                            parent.removeChild(div);
                        }

                    }
                };
                xhr.open("POST", "Archivepcbdd.php", true);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.send('Id=' + JSON.stringify(idpc) + '&Option=' + option);
                
                
            }
             //Recupère la valeur du Sélect puis l'envoie dans retournenbpc puis appelle modifDiv avec le nbdePc en paramètre
  
            function testselect(Numero) {

                var liste;
                liste = document.getElementById("SelectDemande");
                option = liste.options[liste.selectedIndex].text;
                
                
                
                var xhr = getXMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                        modifDiv(xhr.responseText);

                    } else if (xhr.readyState < 4) {
                        
                         if(document.getElementById("reponsexhr")){
                            var div=document.getElementById("reponsexhr");
                            var parent=document.getElementById('formulaire');
                            parent.removeChild(div);
                        }



                    }
                };
                xhr.open("POST", "retournenbpc.php", true);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.send('Option=' + option);
                
                
 
                //Si l'on coche une checkbox, on incrémente le nombre de cases cochées
                if(document.getElementById('check'+Numero).checked===true)
                {
                    cpt++;
             
                   
                }
                else    //Si l'on décoche une case, on décrémente le nombre de cases cochées
                {
                    cpt--;
                }

              
            }
            // Affiche un paragraphe avec le nombre de pc que comprend la demande
            //Affiche un compteur qui s'incrémente selon le nombre de pc sélectionné(s'affiche en rouge si le nombre de pc n'est pas suffisant et en vert si tout est bon)
            //Affiche le bouton Archiver si la couleur de la div précédente est verte sinon ne l'affiche pas (condition : nbPc = nbCheckboxes)
            function modifDiv(nbPc){
                //Fait apparaitre le paragraphe si invisible
                 if(document.getElementById('avertPc').style.display == 'none'){
                    document.getElementById('avertPc').style.display = 'block';
                }
                if(document.getElementById('nbPc').style.display == 'none'){
                    document.getElementById('nbPc').style.display = 'block';
                }
                //Si le compteur n'est pas égal à la demande, on cache le bouton.
                if(cpt != nbPc){
                    document.getElementById('nbPc').style.color = 'red';
                    document.getElementById('btnArchiver').style.display = 'none';
                }
                //Si le comteur est bien égal à la demande, on affiche le bouton.
                else{
                    document.getElementById('nbPc').style.color = 'green';
                    document.getElementById('btnArchiver').style.display = 'inline';
                }
                
                document.getElementById('nbPc').innerHTML = 'PCs séléctionnés :'+cpt+ '/'+ nbPc; //Affichage du nombre de PCs séléctionnés par rapport au nombre demandé
                document.getElementById('avertPc').innerHTML = 'Cette demande comprend '+nbPc+' PCs'; //Affichage du nombre de PCs demandés
            }
            
            
             //Permet de creer la div pour contenir le message reçu après traitement
            function testAlert(text) {

                var DivJum = document.querySelector('.jumbotron');
                var newDiv = document.createElement('div');
                newDiv.className = 'form-group col-md-6 col-centered MarginTop';
                newDiv.id='reponsexhr';
                var newP = document.createElement('p');
                newP.className = 'text-center';
                newDiv.appendChild(newP);

                var newtexte = document.createTextNode(text);
                newP.appendChild(newtexte);
                DivJum.appendChild(newDiv);
               

            }
        </script>
    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../licences/envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="../licences/obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Demandes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ajout_demande.php">Soumettre une demande</a></li>
                                    <li class="active"><a href="terminer_demande.php">Valider une demande</a></li>
                                    <li role="separator" class="divider"></li>
                                  <li><a href='inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>


                    </div>
                </div>
            </nav>

            <div class="jumbotron">
                <h1 class='text-center'>Terminer une demande</h1>
                <p class='text-center' style='font-size: big;'>Vous trouverez ici l'ensemble des PCs réparés</p>
                <p class='text-center' style='font-size: medium;'>
                    Selectionnez la demande, et cochez les pcs à envoyer.
                </p>

                <?php
                require "../../Class/Database.php";
                $pdo = new Database('repair');
                // Affiche l'inventaire des Pcs réparés rangés par version et date d'enregistrement
                function afficheInvent($Nom_champ, $Table) {

                    $pdo = new Database('repair');
                    $N = 0;

                    echo '<table id="inventaire" class="col-md-8 col-centered MarginTop" border="1" cellpadding="2">
                         <thead>
                        <tr>
                        <th><b>Séléction</b></th>
                        <th class="col-md-2"><b>Version</b></th>
                        <th><b>Proco</b></th>
                        <th><b>Marque</b></th>
                        <th><b>Donateur</b></th>
                        <th><b>Description</b></th>
                        <th><b>Emplacement</b></th>
                        <th><b>Date enregistrement</b></th>
                        <th><b>Réparé</b></th>
                        </tr>
                        </thead>
                        <tbody>';
                    foreach ($pdo->query('SELECT * FROM ' . $Table . ' WHERE repare=1 ORDER BY version,' . $Nom_champ . '')as $row) {
                        $N = $N + 1;

                        $id=$row->id;
                        $version = $row->version;
                        $Proco = $row->Proco;
                        $Marque = $row->marque;
                        $Donateur = $row->donateur;
                        $enregistrement = $row->date;
                        $Description = $row->description;
                        $Emplacement = $row->emplacement;
                        $Repare = $row->repare;

                        echo'<tr>
                             <td>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="check' . $N . '" name="check' . $N . '" onClick="testselect('.$N.')">
                                </div>
                            </td>
                             <td id="idligne'.$N.'" style="display:none;">'.$id.'</td>
                             <td>' . $version . '</td>
                             <td>' . $Proco . '</td>
                             <td>' . $Marque . '</td>
                             <td>' . $Donateur . '</td>
                             <td>' . $Description . '</td>
                             <td>' . $Emplacement . '</td>
                             <td>' . $enregistrement . '</td>';

                        if ($Repare) {
                            echo '<td>✔️</td>';
                        } else {


                            echo '<td>❌</td>';
                        }
                    }
                    echo'</tr>';

                    echo '</tbody>
                              </table>';
                    // Affiche le nombre de Pc disponnible
                    $req = $pdo->query("SELECT COUNT(Id) AS count FROM pc WHERE repare = 1")[0];
                    $nbpcdispo = $req->count;
                    echo '<div class="col-md-8 col-centered">';
                    echo '<p style="float:left; font-size:medium;"> Nombre de PC disponibles : </p>';
                    echo '<p id="nbpcdispo" style="font-size:medium;"> '.$nbpcdispo.'</p>';
                    echo '</div>';
                }
                ?>

                <form id="formulaire">
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        echo '<label for="SelectDemande">Selectionnez votre demande</label>';
                        echo '<select name="SelectDemande" class="form-control" id="SelectDemande" onChange="testselect()"  >';
                        $cpt = 0;
                        echo '<option disabled selected value> -- Sélectionner la structure -- </option>';
                        //On compte le nombre de demandes existantes
                        foreach ($pdo->query('SELECT * FROM demandes') as $dem) {
                            $cpt++;
                            echo '<option name="' . $dem->NomStruct . '" value="$option' . $cpt . '">' . $dem->NomStruct . '</option>';
                        }


                        echo '</select>';
                        ?>
                    </div>


                   
                    <p id='avertPc' style='display:none;' class='text-center'>Cette demande comprend B PCs</p>
                    <!-- Tableau s'adaptant a la fenetre (si trop de clés, on scroll) -->
                    <div style="overflow-y:auto;">
                        <?php
                        afficheInvent("date", "pc");
                        ?>
                    </div>
              


                    <!-- Affichage du nombre de PC selectionnés par rapport au nombre requis -->
                    <p id='nbPc' class='text-center MarginTop' style='color:red; display:none;'>PCs séléctionnés :</p>

               
                   
                     <div class="row" id="button">
                        <div class="col-sm-12">
                            <div class="text-center MarginTop">
                                <button type="button" class="btn btn-primary" id="btnArchiver" style='display:none;' onclick="request()"> Archiver</button>
                            </div>
                        </div>
                    </div>
                    
                      <div class="form-group col-md-3 col-centered MarginTop">
                    <span id="image" style="display:none"><img src="../../img/loader.svg" alt="loading"/></span>
                     </div>
                </form>  

            </div>
            
            

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="../../js/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
