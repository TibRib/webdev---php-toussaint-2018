<?php
//Verification de la session.
session_start();
if (isset($_SESSION["login"])) {
    
} else {
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"] === false) {
    header("Location: ../sign_in.php");
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <link rel="stylesheet" href="../../css/inventaire.css" />

        

        
    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../licences/envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="../licences/obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Demandes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ajout_demande.php">Soumettre une demande</a></li>
                                    <li><a href="terminer_demande.php">Valider une demande</a></li>
                                    <li role="separator" class="divider"></li>
                                  <li class="active"><a href='inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>

                    </div>
                </div>
            </nav>

            <div class="jumbotron">
                <h1 class='text-center'>Archive</h1>
                <p class='text-center' style='font-size: medium;'>Vous trouverez ici l'ensemble des PCs archivés</p>
                

                    <?php
                    require "../../Class/Database.php";
                    $pdo = new Database('repair');

                    //Affiche l'inventaire contenant tous les pc archivés
                    function afficheArchive($Nom_champ, $Table) {

                        $pdo = new Database('repair');
                        $N = 0;

                        echo '<table id="inventaire" class="col-md-8 col-centered" border="1" cellpadding="2">
                         <thead>
                        <tr>
                        <th><b>N°</b></th>
                        <th><b>Proco</b></th>
                        <th><b>Marque</b></th>
                        <th><b>Donateur</b></th>
                        <th><b>Date enregistrement</b></th>
                        <th><b>Date archivation</b></th>
                        <th><b>Destination</b></th>

                        </tr>
                        </thead>
                        <tbody>';
                        foreach ($pdo->query('SELECT * FROM ' . $Table . '  ORDER BY ' . $Nom_champ . ' ')as $row) {
                            $N = $N + 1;


                            $Proco = $row->Proco;
                            $Marque = $row->marque;
                            $Donateur = $row->donateur;
                            $enregistrement = explode(' ',$row->date)[0];   //On separe la date et l'heure pour affichage
                            $DateArchive = explode(' ',$row->datearchivation)[0];   //Pareil, on ne garde que la date.
                            $Dest = $row->destination;

                            echo'<tr>
                             <td>' . $N . '</td>
                             <td>' . $Proco . '</td>
                             <td>' . $Marque . '</td>
                             <td>' . $Donateur . '</td>
                             <td id="DateEnregistrement' . $N . '">' . $enregistrement . '</td>
                             <td id="DateArchive' . $N . '">' . $DateArchive . '</td>
                             <td>' . $Dest . '</td>';

                        }
                        echo'</tr>';

                        echo '</tbody>
                              </table>';
                    }

                    ?>
                
                <!-- Tableau s'adaptant a la fenetre (si trop de clés, on scroll) -->
                <div style="overflow-y:auto;" class="MarginTop">
                    <?php
                    afficheArchive("destination","archivepc");
                    ?>
                </div>
                
                
                
            </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="../../js/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
