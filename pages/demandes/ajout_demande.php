<?php
//Verification de la session.
session_start();
if (isset($_SESSION["login"])) {
    
} else {
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"] === false) {
    header("Location: ../sign_in.php");
    die();
}

//Si le role est trop bas
if ($_SESSION["role"] < 2) {
    header("Location: ../non_autorise.php");
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ajouter une demande | Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <?php
        require '../../Class/form.php';
        $form = new Form($_POST);
        ?>
        
         <script src="../../js/ObjetXHR.js" type="text/javascript"></script>

        <script type="text/javascript">
            
            //Verifie si tous les champs sont remplis sauf les 4 derniers(non nécessaire) puis récupère la valeur de chaque champ
            //Envoie toutes ces informations dans ajout_demandebdd.php pour le traitement en affichant l'image de chargement puis une div avec le message reçu(supprimer à chaque envoie)
            function request(callback) {
               
                if (document.getElementById('InputStructure').checkValidity() && document.querySelector("select[name='SelectType']").checkValidity()
                        && document.getElementById('InputPostal').checkValidity() 
                        && document.getElementById('InputNom').checkValidity() && document.getElementById('InputTel').checkValidity() && document.getElementById('InputMail').checkValidity() 
                         && document.getElementById('InputProjet').checkValidity()) {

                    var structure = document.getElementById("InputStructure").value;
                    var type = document.querySelector("select[name='SelectType']").value;
                    var postal = document.getElementById("InputPostal").value;
                    var nom = document.getElementById("InputNom").value;
                    var tel = document.getElementById("InputTel").value;
                    var Mail = document.getElementById("InputMail").value;
                    var Projet = document.getElementById("InputProjet").value;
                    var nbpc=document.getElementById("InputNbPc").value;
                    var nbsouris=document.getElementById("InputNbSouris").value;
                    var nbclavier=document.getElementById("InputNbClavier").value;
                    var nbecran=document.getElementById("InputNbEcran").value;
                    if(type==="Autre")
                    {
                        if(document.getElementById("InputAutreStruct").checkValidity()){
                        type=document.getElementById("InputAutreStruct").value;
                    }
                    else
                    {
                        
                        alert("Préciser le type de structure");
                        return;
                        
                    }
                       
                    }
                   
                    var xhr = getXMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                            callback(xhr.responseText);
                            document.getElementById("image").style.display = "none";
                        } else if (xhr.readyState < 4) {
                            document.getElementById("image").style.display = "inline";
                            if(document.getElementById("reponsexhr")){
                            var div=document.getElementById("reponsexhr");
                            var parent=document.querySelector('.jumbotron');
                            parent.removeChild(div);
                        }

                        }
                    };
                    xhr.open("POST", "ajout_demandebdd.php", true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send('InputStructure=' + structure + '&SelectType=' + type + '&InputPostal=' + postal + '&InputNom=' + nom + '&InputTel=' + tel
                            + '&InputMail=' + Mail + '&InputProjet=' + Projet +'&InputNbPc=' + nbpc  + '&InputNbSouris=' + nbsouris + '&InputNbClavier=' + nbclavier + '&InputNbEcran=' + nbecran);
                } else {
                    alert("Remplissez correctement le formulaire ;)");
                }
            }
            
            //Permet de creer la div pour contenir le message reçu après traitement
            function testAlert(text) {

                var DivJum = document.querySelector('.jumbotron');
                var newDiv = document.createElement('div');
                newDiv.className = 'form-group col-md-6 col-centered MarginTop';
                newDiv.id='reponsexhr';
                var newP = document.createElement('p');
                newP.className = 'text-center';
                newDiv.appendChild(newP);

                var newtexte = document.createTextNode(text);
                newP.appendChild(newtexte);
                DivJum.appendChild(newDiv);
               

            }
            
            //Permet d'afficher un input text en plus si le choix du select vaut autre sinon il ne l'affiche pas
            function testselect(){
                 
                if(document.querySelector("select[name='SelectType']").value==="Autre")
                {
                    document.getElementById('Divautre').style.display = 'block'; 
                    
                }
                else
                {
                    
                    document.getElementById('Divautre').style.display = 'none'; 
                }
                
            }
            
            </script>
    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../licences/envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="../licences/obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../stocks/enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../stocks/inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>
                            
                            
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  Demandes <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                  <li class="active"><a href="ajout_demande.php">Soumettre une demande</a></li>
                                  <li><a href="terminer_demande.php">Valider une demande</a></li>
                                  <li role="separator" class="divider"></li>
                                  <li><a href='inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>


                    </div>
                </div>
            </nav>

            <div class="jumbotron">

                <h1 class="text-center">Ajouter une demande</h1>
                <p class='text-center' style='font-size: medium;'>Enregistrer une demande de don d'un organisme ou d'une personne</p>
                <form action='ajout_demandebdd.php' method="post">

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input("Nom de la structure", 'InputStructure', 'Ex: Ecole Jean-Moulin', 'form-control');
                        ?>
                    </div>
                    
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                            //$champs = ['txt','value'];
                            
                            $option1 = ['Etablissement scolaire','Etablissement scolaire'];
                            $option2 = ['Association','Association'];
                            $option3 = ['Autre','Autre'];
                            
                            $choix = [$option1, $option2, $option3];
                            
                            $form->select("Type de structure", 'SelectType', 'form-control',$choix,'testselect()');
                            
                           
                                echo '</div><div id="Divautre" style="display:none" class="form-group col-md-5 col-centered MarginTop">';
                                $form->input("Préciser le type:", 'InputAutreStruct', '...', 'form-control');
                            
                        ?>
                    </div>
                    
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input("Adresse postale", 'InputPostal', 'Ex: 83000', 'form-control',null,'text','[0-9]{5}');
                        ?>
                    </div>
                    
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input("Nom et prénom du responsable", 'InputNom', 'Nom Prénom', 'form-control',null,'text');
                        ?>
                    </div>
                    
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input("N° de téléphone du responsable", 'InputTel', 'Tel..', 'form-control',null,'tel',"^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$");
                        ?>
                    </div>
                    
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input("Adresse mail du responsable", 'InputMail', 'Email..', 'form-control',null,'email');
                        ?>
                    </div>
                    
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input("Projet à réaliser", 'InputProjet', '...', 'form-control',null,'textarea');
                        ?>
                    </div>
                    

                    <div class="form-group col-md-6 col-centered MarginTop">

                        <label>Nature de la demande</label>
                        
                        <div class="form-group row flex-v-center">
                            <div class="col-sm-2 text-center">
                                <span style="line-height: 2.5">PC:</span>
                            </div>
                            
                            <div class="col-sm-2">
                                  <input type="number" class="form-control" name='InputNbPc' id='InputNbPc' value='0' min='0'/>
                            </div>
                            
                            <div class="col-sm-2 text-center">
                                <span style="line-height: 2.5">Ecrans:</span>
                            </div>
                            
                            <div class="col-sm-2">
                                  <input type="number" class="form-control" name='InputNbEcran' id='InputNbEcran' value='0' min='0'/>
                            </div>
                            
                        </div>
                    </div>
                        
                    <div class="form-group col-md-6 col-centered MarginTop">


                        <div class="form-group row flex-v-center">
                            <div class="col-sm-2 text-center">
                                <span style="line-height: 2.5">Claviers:</span>
                            </div>
                            
                            <div class="col-sm-2">
                                  <input type="number" class="form-control" name='InputNbClavier' id='InputNbClavier' value='0' min='0'/>
                            </div>
                            
                            <div class="col-sm-2 text-center">
                                <span style="line-height: 2.5">Souris:</span>
                            </div>
                            
                            <div class="col-sm-2">
                                  <input type="number" class="form-control" name='InputNbSouris' id='InputNbSouris' value='0' min='0'/>
                            </div>
                            
                        </div>
                    </div>

                    <div class="text-center MarginTop">
                        <?php
                        $form->submit('btn btn-primary', 'Enregistrer', 'request(testAlert)');
                        ?>
                    </div>
                    
                     <div class="form-group col-md-3 col-centered MarginTop">
                    <span id="image" style="display:none"><img src="../../img/loader.svg" alt="loading"/></span>
                     </div>
                </form>

            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
