<?php

//Verification de la session.
session_start();
if(isset($_SESSION["login"])){}
else{
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"]===false){
    header("Location: ../sign_in.php");
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>
        
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <?php
            require "../../Class/Database.php";
            $db= new Database('repair');
        ?>
    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                      <span class="sr-only">Naviguer</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../../index.php">Repair</a>
                  </div>

                  <!-- Contenu -->
                  <div class="collapse navbar-collapse" id='collapse-1'>
                      <!-- Liste des boutons clickables du menu -->
                    <ul class="nav navbar-nav">
                        <li><a href="../../index.php">Accueil</a></li>

                      <!-- Element déroulant : class = "dropdown" -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Licences <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="../licences/envoyer_cle.php">Envoyer une clé</a></li>
                          <li><a href="../licences/obtenir_cle.php">Obtenir une clé</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="../licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                        </ul>
                      </li>
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Stocks <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="enregistrer_pc.php">Enregistrer un PC</a></li>
                          <li role="separator" class="divider"></li>
                          <li class="active"><a href="demandes_et_dispo.php">PC prêts et à faire</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="inventaire_pc.php">Inventaire des PC</a></li>
                        </ul>
                      </li>
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Demandes <span class="caret"></span></a>
                         <ul class="dropdown-menu">
                             <li><a href="../demandes/ajout_demande.php">Soumettre une demande</a></li>
                             <li><a href="../demandes/terminer_demande.php">Valider une demande</a></li>
                             <li role="separator" class="divider"></li>
                             <li><a href='../demandes/inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>

                      
                  </div>
                </div>
              </nav>
            
            <div class="jumbotron">
                <span class="text-big">
                    <span class="text-super-big glyphicon glyphicon-wrench"></span>
                    <span class="text-ultra-big">
                        <!-- Valeur affichee : nombre de pcs a preparer -->
                        <?php
                            $req = $db->query("SELECT SUM(NbPc) AS sum FROM demandes")[0];
                            $demandes = $req->sum;
                            if(isset($demandes)){
                            echo $demandes;
                            }
                            else{
                                echo '0';
                            }
                        ?>
                    </span>
                    PCs à préparer.
                </span>
                <div id="hide">
                    <b>Destinataires :</b>
                    <ul>
                        <?php
                        
                            foreach ($db->query("SELECT * FROM demandes") as $dem) {
                                echo '<li>'.$dem->NomStruct.' ('.$dem->zip.') : '.$dem->NbPc.' PC</li>';
                            }

                         
                        ?>
                    </ul>
                </div>
                
            </div>
            
            <div class="jumbotron">
                <span class="text-big">
                    <span class="text-super-big glyphicon glyphicon-gift"></span>
                    <span class="text-ultra-big">
                        <!--Affiche le nombre de pc qui sont réparés et prêts à être donnés-->
                        <?php
                            $req = $db->query("SELECT COUNT(Id) AS count FROM pc WHERE repare = 1")[0];
                            echo $req->count;
                        ?>
                    </span>PCs prêts pour être donnés
                </span>
                <div id="hide">
                    <b>Séries :</b>
                    <ul>
                        <?php
                            $marque = "";
                            $nb = 0;
                            
                            /* Affichage des pcs par série */
                            foreach ($db->query("SELECT * FROM pc WHERE repare=1 GROUP BY marque") as $req) {   //Pour chaque marque de pc,
                                
                                $marque = $req->marque;
                                $nbpc = $db->query("SELECT COUNT(Id) AS count FROM pc WHERE marque ='".$marque."'AND repare=1 ")[0]->count;  // On compte le nombre de pc qui possèdent cette marque
                                
                                for($i = 0; $i < $nbpc; $i++){  //tant que le nombre n'est pas atteint, on incremente un compteur
                                    $nb++;
                                }
                                echo '<li>'.$marque.': '.$nb.'</li>';   //Une fois atteint, on affiche la marque et le nombre de pcs.
                                $nb = 0;    //Reinitialisation du compteur
                            }
                        ?>
                    </ul>
                </div>
                
            </div>
        </div>
        
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
