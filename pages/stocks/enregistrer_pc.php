<?php
//Verification de la session.
session_start();
if (isset($_SESSION["login"])) {
    
} else {
    $_SESSION["login"] = false;
}

//Si l'utilisateur n'est pas connecté, on le redirige vers la page de connexion.
if ($_SESSION["login"] === false) {
    header("Location: ../sign_in.php");
    die();
}

//Si le role est trop bas
if ($_SESSION["role"] < 1) {
    header("Location: ../non_autorise.php");
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repair</title>

        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css" />
        <link rel="icon" href="../../img/favicon.ico" />
        <link rel="stylesheet" href="../../css/styles.css" />
        <?php
        require '../../Class/form.php';
        $form = new Form($_POST);
        ?>

        <script src="../../js/ObjetXHR.js" type="text/javascript"></script>

        <script type="text/javascript">
              //Verifie si tous les champs sont remplis correctement puis récupère la valeur de chaque champ
            //Envoie toutes ces informations dans envoiepcbdd.php pour le traitement en affichant l'image de chargement puis une div avec le message reçu(supprimer à chaque envoie)
            function request(callback) {
               
                if (document.getElementById('InputDonateur').checkValidity() && document.querySelector("input[name='optionsRadios']").checkValidity()
                        && document.getElementById('InputMarque').checkValidity() 
                        && document.getElementById('InputProc').checkValidity() && document.getElementById('InputEmplacement').checkValidity() && document.getElementById('InputComm').checkValidity()) {
             
                var donateur = document.getElementById("InputDonateur").value;
                    var option = document.querySelector("input[name='optionsRadios']:checked").value;
                    var marque = document.getElementById("InputMarque").value;
                    var Date = document.querySelector("input[name='InputDate']").value;
                    var Proco = document.getElementById("InputProc").value;
                    var Emplacement = document.getElementById("InputEmplacement").value;
                    var Comm = document.getElementById("InputComm").value;
                    var xhr = getXMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                            callback(xhr.responseText);
                            document.getElementById("image").style.display = "none";
                        } else if (xhr.readyState < 4) {
                            document.getElementById("image").style.display = "inline";
                            if(document.getElementById("reponsexhr")){
                            var div=document.getElementById("reponsexhr");
                            var parent=document.querySelector('.jumbotron');
                            parent.removeChild(div);
                        }

                        }
                    };
                    xhr.open("POST", "envoiepcbdd.php", true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send('InputDonateur=' + donateur + '&Option=' + option + '&InputMarque=' + marque + '&InputDate=' + Date + '&InputProc=' + Proco + '&InputEmplacement=' + Emplacement + '&InputComm=' + Comm);
                } else {
                    alert("Remplissez correctement le formulaire ;)")
                }
            }
             //Permet de creer la div pour contenir le message reçu après traitement
            function testAlert(text) {

                var DivJum = document.querySelector('.jumbotron');
                var newDiv = document.createElement('div');
                newDiv.className = 'form-group col-md-6 col-centered MarginTop';
                newDiv.id='reponsexhr';
                var newP = document.createElement('p');
                newP.className = 'text-center';
                newDiv.appendChild(newP);

                var newtexte = document.createTextNode(text);
                newP.appendChild(newtexte);
                DivJum.appendChild(newDiv);
               

            }




        </script>
    </head>
    <body>
        <div class="container">
            <br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Le label et le menu déroulant resteront groupés pour l'affichage mobile -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
                            <span class="sr-only">Naviguer</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../../index.php">Repair</a>
                    </div>

                    <!-- Contenu -->
                    <div class="collapse navbar-collapse" id='collapse-1'>
                        <!-- Liste des boutons clickables du menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="../../index.php">Accueil</a></li>

                            <!-- Element déroulant : class = "dropdown" -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Licences <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../licences/envoyer_cle.php">Envoyer une clé</a></li>
                                    <li><a href="../licences/obtenir_cle.php">Obtenir une clé</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="../licences/inventaire_cle.php">Afficher l'inventaire des clés</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Stocks <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="enregistrer_pc.php">Enregistrer un PC</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="demandes_et_dispo.php">PC prêts et à faire</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="inventaire_pc.php">Inventaire des PC</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Demandes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../demandes/ajout_demande.php">Soumettre une demande</a></li>
                                    <li><a href="../demandes/terminer_demande.php">Valider une demande</a></li>
                                <li role="separator" class="divider"></li>
                             <li><a href='../demandes/inventaire_archive.php'>Archive des envois</a></li>
                              </ul>
                            </li>
                      

                            <li><a href='../timeline.php'>Suivi & Historique</a></li>         
                            <li><a href='https://tasks.office.com/yncrea.fr/fr-FR/Home/Planner#/plantaskboard?groupId=e7508ade-2a50-4ade-9a23-686bdacc78e0&planId=ng27SbiVBkav3qXu6Hd845YABxsR'>
                                    <img style='max-width: 20px' src='../../img/Microsoft-Teams.png' alt='Microsoft' />
                                    Teams
                                </a></li>    
                        </ul>

                      <ul class="nav navbar-nav" id="lieninscr">
                      <?php if($_SESSION["role"] > 1) :?>
                          <li>
                              <a href="../inscription/inscrire.php">Inscrire <span class="glyphicon glyphicon-user"></span></a>

                          </li>
                    <?php endif; ?>
                          <li>
                              <a href="../../logout.php" id="imgout"><img src="../../img/exit-icon.png" width="16" height="16"> Déconnexion</a>
                          </li>
                        </ul>


                    </div>
                </div>
            </nav>

            <div class="jumbotron">

                <h1 class="text-center">Enregistrer un PC</h1>
                <form action='envoiepcbdd.php' method="post">
                    <fieldset class="form-group MarginTop">
                        <legend class="text-center">Quel est son système d'exploitation ?</legend>
                        <div class="col-md-5 col-centered">
                            <!-- Clés anciennes (Legacy) -->
                            <div class="col-md-6">
                                <div class="form-check">
                                    <?php
                                    $form->input('Autre/Linux', 'optionsRadios', '', 'form-check-input', 'Autre', 'radio');
                                    ?>
                                    <!--                                  <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input" name="optionsRadios" id="autre" value="autre">
                                                                        Autre/Linux
                                                                      </label>-->
                                </div>
                                <div class="form-check">
                                    <?php
                                    $form->input('Windows XP', 'optionsRadios', '', 'form-check-input', 'Windows XP', 'radio');
                                    ?>
                                    <!--                                  <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input" name="optionsRadios" id="windowsxp" value="option1">
                                                                        Windows XP
                                                                      </label>-->
                                </div>
                                <div class="form-check">
                                    <?php
                                    $form->input('Windows Vista', 'optionsRadios', '', 'form-check-input', 'Windows Vista', 'radio');
                                    ?>
                                    <!--                                  <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input" name="optionsRadios" id="windowsvista" value="option2">
                                                                        Windows Vista
                                                                      </label>-->
                                </div>
                            </div>

                            <!-- Clés récentes (Actual) -->
                            <div class="col-md-6">
                                <div class="form-check">
                                    <?php
                                    $form->input('Windows 7', 'optionsRadios', '', 'form-check-input', 'Windows 7', 'radio');
                                    ?>
                                    <!--                                  <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input" name="optionsRadios" id="windows7" value="option3" checked>
                                                                        Windows 7
                                                                      </label>-->
                                </div>
                                <div class="form-check">
                                    <?php
                                    $form->input('Windows 8', 'optionsRadios', '', 'form-check-input', 'Windows 8', 'radio');
                                    ?>
                                    <!--                                  <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input" name="optionsRadios" id="windows8" value="option4">
                                                                        Windows 8
                                                                      </label>-->
                                </div>
                                <div class="form-check">
                                    <?php
                                    $form->input('Windows 10', 'optionsRadios', '', 'form-check-input', 'Windows 10', 'radio');
                                    ?>
                                    <!--                                  <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input" name="optionsRadios" id="windows10" value="option5">
                                                                        Windows 10
                                                                      </label>-->
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('D\'où provient le PC / Qui l\'a donné ?', 'InputDonateur', 'Ex: Mairie ; Veolia ; Particulier', 'form-control');
                        ?>
                        <!--                      <label for="InputDonateur">D'où provient le PC / Qui l'a donné ?</label>
                                              <input type="text" class="form-control" id="InputDonateur" placeholder="Ex: Mairie ; Veolia ; Particulier">-->
                    </div>

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('Préciser le Processeur du PC', 'InputProc', 'Ex:intel core 2 Duo', 'form-control');
                        ?>

                    </div>
                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('Préciser la marque du PC', 'InputMarque', 'Ex: HP; DELL; Custom', 'form-control');
                        ?>
                        <!--                      <label for="InputMarque">Préciser la marque du PC</label>
                                              <input type="text" class="form-control" id="InputMarque" placeholder="Ex: HP; DELL; Custom">-->
                    </div>

                    <div class="form-group col-md-6 col-centered MarginTop">

                        <label for="InputDate">Entrer une date de réception ou laisser vide si aujourd'hui:</label>
                        <input placeholder=<?php
                        date_default_timezone_set('Europe/Paris');
                        echo '"';
                        echo date('d') . '/' . date('m') . '/' . date('y');
                        echo '"';
                        ?> class="form-control" type="text" onfocus="(this.type = 'date')" name="InputDate">
                    </div>

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('Commentaire :', 'InputComm', '...', 'form-control', '', 'textarea');
                        ?>
                    </div>

                    <div class="form-group col-md-6 col-centered MarginTop">
                        <?php
                        $form->input('Emplacement :', 'InputEmplacement', 'Ex: -1', 'form-control');
                        ?>

                    </div>

                    <div class="text-center MarginTop">
                        <?php
                        $form->submit('btn btn-primary', 'Inscrire ce PC', 'request(testAlert)');
                        ?>
                        <!--                    <button type="submit" class="btn btn-primary">Inscrire ce PC</button>-->
                    </div>
                     <div class="form-group col-md-3 col-centered MarginTop">
                    <span id="image" style="display:none"><img src="../../img/loader.svg" alt="loading"/></span>
                     </div>
                </form>

            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
