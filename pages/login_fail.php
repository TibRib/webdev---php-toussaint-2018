<?php
    //Pour permettre aux developpeurs de sortir d'une session en se rendant sur cette page
    session_start(); 
    $_SESSION["login"] = false;
    
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Connexion | Repair</title>
        <link rel="icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
        <link href="../css/signin.css" rel="stylesheet">
    </head>

  <body class="text-center">
      
    <div class='container'>
      <img class="mb-4" src="../img/logo_repair_big.png" alt="ISEN REPAIR" height="132">
      <h1 class="h3">Désolé mais votre identifiant ou votre mot de passe n'ont pas été reconnus</h1>
      <h2 class="h4">Merci de vous adresser à l'un des administrateurs directement à l'association, ou de les contacter par mail.</h2>
      <br>
      <br>
      <a class="btn btn-primary" href="../index.php" role="button">Retour vers l'écran de connexion</a>
      <br>
      <br>
      <p class="mt-5 mb-3 text-muted">&copy; 2018-2019 Thibaud SIMON & Valentin Volpelliere</p>
    </div> 
  </body>

</html>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
