<?php 

 require 'Class/Database.php';
 $db = new Database('repair');
 
// Pour chaque ligne de la table comptes
foreach( $db->query('SELECT * FROM comptes')as $row){

$hashed_password = $row->password;
$role = $row->role;
if($role === NULL ){ $role = 0; }
$password = $_POST["password"];

//On compare l'entrée utilisateur au mot de passe en hash de la ligne correspondante dans la bdd.
if((password_verify($password, $hashed_password)) && ($_POST["nom"]==$row->pseudo)){
    session_start();
    $_SESSION["login"] = true;
    $_SESSION["role"] = $role;
    header('Location: index.php');
    die();
    
}


}
//Redirection en cas d'échec
 header('Location: pages/login_fail.php');
 
 ?>
